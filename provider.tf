terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region     = var.region
  access_key = "<Update your AWS key>"
  secret_key = "<Update your AWS Secret key>"
}
