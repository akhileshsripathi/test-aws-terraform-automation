# vpc
resource "aws_vpc" "demo_vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames=true

  tags = {
    Name = var.vpc_tag
  }
}

# public subnet
resource "aws_subnet" "demo_public_subnet" {
  vpc_id     = aws_vpc.demo_vpc.id
  cidr_block = var.public_cidr

  availability_zone_id = var.public_az

  tags = {
    Name = var.public_subnet_tag
  }

  map_public_ip_on_launch = true
}


# private subnet
resource "aws_subnet" "demo_private_subnet" {
 
  vpc_id     = aws_vpc.demo_vpc.id
  cidr_block = var.private_subnet_cidr

  availability_zone_id = var.private_subnet_az

  tags = {
    Name = var.private_subnet_tag
  }
}

# internet gateway
resource "aws_internet_gateway" "demo_internet_gateway" {
 
  vpc_id = aws_vpc.demo_vpc.id

  tags = {
    Name = var.internet-gateway_tag
  }
}


# route table with target as internet gateway
resource "aws_route_table" "IG_route_table" {
  depends_on = [
    aws_vpc.demo_vpc,
    aws_internet_gateway.demo_internet_gateway,
  ]

  vpc_id = aws_vpc.demo_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo_internet_gateway.id
  }

  tags = {
    Name = "IG-route-table"
  }
}


# associate route table to public subnet
resource "aws_route_table_association" "associate_routetable_to_public_subnet" {
  depends_on = [
    aws_subnet.demo_public_subnet,
    aws_route_table.IG_route_table,
  ]
  subnet_id      = aws_subnet.demo_public_subnet.id
  route_table_id = aws_route_table.IG_route_table.id
}

# elastic ip
resource "aws_eip" "elastic_ip" {
  vpc      = true
}

# NAT gateway
resource "aws_nat_gateway" "nat_gateway" {
  depends_on = [
    aws_subnet.demo_public_subnet,
    aws_eip.elastic_ip,
  ]
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = aws_subnet.demo_public_subnet.id

  tags = {
    Name = var.nat_gateway
  }
}

# route table with target as NAT gateway
resource "aws_route_table" "NAT_route_table" {
  depends_on = [
    aws_vpc.demo_vpc,
    aws_nat_gateway.nat_gateway,
  ]

  vpc_id = aws_vpc.demo_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway.id
  }

  tags = {
    Name = "NAT-route-table"
  }
}


# associate route table to private subnet
resource "aws_route_table_association" "associate_routetable_to_private_subnet" {
  depends_on = [
    aws_subnet.demo_private_subnet,
    aws_route_table.NAT_route_table,
  ]
  subnet_id      = aws_subnet.demo_private_subnet.id
  route_table_id = aws_route_table.NAT_route_table.id
}

# bastion host security group
resource "aws_security_group" "sg_bastion_host" {
  depends_on = [
    aws_vpc.demo_vpc,
  ]
  name        = var.security_group_name_first
  description = "bastion host security group"
  vpc_id      = aws_vpc.demo_vpc.id

  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# public instance security group
resource "aws_security_group" "sg_public_group" {
  depends_on = [
    aws_vpc.demo_vpc,
  ]

  name        = var.sg_public
  description = "Allow http inbound traffic"
  vpc_id      = aws_vpc.demo_vpc.id

  ingress {
    description = "allow TCP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.sg_bastion_host.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# database security group
resource "aws_security_group" "sg_private" {
  depends_on = [
    aws_vpc.demo_vpc,
  ]
  name        = "sg private"
  description = "Allow database inbound traffic"
  vpc_id      = aws_vpc.demo_vpc.id

  ingress {
    description = "allow TCP"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.sg_public_group.id]
  }

  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.sg_bastion_host.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
