variable "region" {
  description = "AWS Deployment region.."
  default = "us-west-2"
}

variable "public_subnet_tag" {
  description = "AWS Deployment region.."
  default = "dev-public-subnet"

}

variable "private_subnet_tag" {
  description = "AWS Deployment region.."
  default = "dev-private-subnet"

}

variable "internet-gateway_tag" {
  description = "AWS Deployment region.."
  default = "demo-internet-gateway"

}

variable "private_subnet_cidr" {
  description = "AWS Deployment region.."
  default = "192.168.1.0/24"

}

variable "private_subnet_az" {
  description = "AWS Deployment region.."
  default = "usw2-az2"

}

variable "vpc_cidr" {
  description = "To Set the VPC default value"
  default = "192.168.0.0/16"

}

variable "vpc_tag" {
  description = "To set the VPC tag for AWS network"
  default = "Demo AWS Network"

}

variable "public_subnet" {
  description = "To set the VPC Name for AWS network"
  default = "demo_public_subnet"

}
variable "vpc_name" {
  description = "To set the VPC Name for AWS network"
  default = "demo_vpc"

}

variable "public_cidr" {
  description = "To Set the public subnet cidr default value"
  default = "192.168.0.0/24"

}

variable "public_az" {
  description = "To Set the public subnet Availability Zone value"
  default = "usw2-az1"

}

variable "nat_gateway" {
  description = "To Set up public gateway"
  default = "nat-gateway"

}

variable "security_group_name_first" {
  description = "To Set up public gateway"
  default = "sg bastion host"

}

variable "sg_public" {
  description = "To Set up public gateway"
  default = "sg_public"

}

variable "sg_private" {
  description = "To Set up public gateway"
  default = "sg_private"

}
